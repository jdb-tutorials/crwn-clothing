import React from "react";
import StripeCheckout from "react-stripe-checkout";

const StripeCheckoutButton = ({ price }) => {
  const centsPrice = price * 100;
  const pubKey =
    "pk_test_51JPIUnB4GLLbwxFjdfgX1aigIlI66Zw8RMCeGrKoF6P8KBrIm6dNnAl2fo4Ii5U5yrXSfqpJLuP5j3bibigtj8mz00Pz4LUd8H";

  const onToken = (token) => {
    console.log(token);
    alert("Payment Successful");
  };

  return (
    <StripeCheckout
      label="Pay Now"
      name="Crown Clothing Ltd."
      billingAddress
      shippingAddress
      image="https://svgshare.com/i/CUz.svg"
      description={`Your total is $${price}`}
      amount={centsPrice}
      panelLabel="Pay Now"
      token={onToken}
      stripeKey={pubKey}
    />
  );
};

export default StripeCheckoutButton;
