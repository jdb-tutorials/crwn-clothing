import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyBh7PBbdcx1M-GE3AyY64bhzhKACP-Rwkc",
  authDomain: "crwn-db-43e0c.firebaseapp.com",
  projectId: "crwn-db-43e0c",
  storageBucket: "crwn-db-43e0c.appspot.com",
  messagingSenderId: "960514393094",
  appId: "1:960514393094:web:b8e3cc4b7d49bf7c232f87",
  measurementId: "G-81T0QKBBTY",
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();
  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({ displayName, email, createdAt, ...additionalData });
    } catch (error) {
      console.log("Error Creating User", error.message);
    }
  }
  return userRef; 
};


const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
