# crwn-clothing

Advanced React/Redux/GraphQL project from Udemy & [Zero-to-Mastery's **Complete React Developer in 2021**](https://www.udemy.com/course/complete-react-developer-zero-to-mastery/)

The authors recommended a GitHub to Heroku deployment. I've since migrated to using GitLab heavily. A great example of GitLab CI/CD - Heroku [integration is here](https://medium.com/@oliver.kocsis/deploy-app-to-heroku-from-gitlab-209983635f3).